'use strict'

//const Worker = require('node:worker_threads');
//const http = require('node:http');
//const url = require('node:url');
//const os = require('node:os');
//
//console.log(Worker);

import {Worker} from 'node:worker_threads';
import http from 'node:http';
import url from 'node:url';
import os from 'node:os';

let thread_options = {
	id: 0,
	port: 8081,
	host: '0.0.0.0'
};

const workers_data = [];
const workers = [];
const connections = [];

for(let i = 0; i < os.cpus().length / 2; i++) {
	workers_data.push(
		{
			id: i + 1,
 			port: 8000 + i + 1,
 			host: '127.0.0.1',
		}
	)
}

workers_data.forEach((data) => {
    workers.push(
        new Worker('./src/worker.js', { workerData: data })
    );
    connections.push(0);
})

let connection_index = -1;

console.clear();
const parseIncomingRequest = (clientRequest, clientResponse) => {
    const requestToFulfil = url.parse(clientRequest.url);

    const min_connections = Math.min(...connections);
    connection_index = connections.indexOf(min_connections);
    if(connection_index < 0 && connections.length) { connection_index = 0; }

    // Frame the request to be forwarded via Backend to External Source
    const options = {
		method: clientRequest.method,
		headers: clientRequest.headers,
		host: requestToFulfil.hostname || workers_data[connection_index].host,
		port: requestToFulfil.port || workers_data[connection_index].port,
		path: requestToFulfil.path
    };

    executeRequest(options, clientRequest, clientResponse);

}

const executeRequest = (options, clientRequest, clientResponse) => {
	connections[connection_index]++;
	console.clear();
	//console.dir(connections);

    const externalRequest = http.request(options, (externalResponse) => {
        // Write Headers to clientResponse
        clientResponse.writeHead(externalResponse.statusCode, externalResponse.headers);
        // Forward the data being received from external source back to client
        externalResponse.on("data", (chunk) => {
            clientResponse.write(chunk);

        });

        // End the client response when the request from external source has completed
        externalResponse.on("end", () => {
            clientResponse.end();
			connections[connection_index] --;

			if(connection_index > 0)
				connection_index--;
			else
				connection_index = connections.length - 1;

			//console.clear();
			console.dir(connections);
        });
    });

    // Map data coming from client request to the external request being made
    clientRequest.on("data", (chunk) => {
        externalRequest.write(chunk);
    });

    // Map the end of client request to the external request being made
    clientRequest.on("end", () => {
        externalRequest.end();
    });
}

// Create a HTTP server
const server = http.createServer(parseIncomingRequest);

// Listen to PORT 4998
server.listen(thread_options.port, thread_options.host, () => {
	console.log(`Server running at http://${thread_options.host}:${thread_options.port}/`);
});
