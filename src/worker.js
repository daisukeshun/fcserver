'use strict'
import {workerData} from 'node:worker_threads';
import http from 'node:http';
import fs from 'node:fs';
import {extname} from 'node:path';
import path from 'node:path';
//todo: заменить на async/await sqlite
import sqlite3 from 'sqlite3';

import Route from './route.js';

const thread_options = workerData;
const settings = JSON.parse(fs.readFileSync("./settings.json"));
const chunk_limit = 1024 * 1024 * settings.chunkLimit; 


/** 
 * @param {Request} req 
 * @param {Response} res 
 */
async function processGet(req, res){
	//const response_file = await getPath(req.url);
	const response_file = './pages/main.html'

	//if(!response_file){
		//res.writeHead(404, "File not found");
		//res.write("File not found");
		//res.end();
		//return;
	//}

	//getMime(response_file).then((mime) => {
		//res.setHeader('Content-Type', mime);
	//});

	res.setHeader('Content-Type', 'text/html');

	//асинхронная проверка на размер файла и после этого создается поток на чтение
	fs.stat(response_file, async (err, fileStats) => {
		if(fileStats.size > settings.maxFileSize * 1024 * 1024) {
			console.log(fileStats);
			res.write("File to large");
			res.end();
			return;
		}

		const read_stream = fs.createReadStream(response_file, "utf8");

		read_stream.on('open', async () => res.writeHead(200, "OK") )
		read_stream.on('data', async (data) => res.write(data));
		read_stream.on('error', async (err) => {
				console.log(err);
				res.writeHead(404, "Page not found");
		})
		read_stream.on('close', async () => {
			res.end();
		});
	})
}

/** 
 * @param {Request} req 
 * @param {Response} res 
 */
async function processPost(req, res){
	const received_chunks = [];
	let size = 0;
	req.on('data', async (chunk) => {
		received_chunks.push(chunk);
		size += chunk.length;
		if(size > chunk_limit){
			req.connection.destroy();
		}
	})
	req.on('end', async () => {
		//console.log(size);
		//обработка POST запроса при завершении получения данных с клиента
		const received_bytes = Buffer.concat(received_chunks);
		const req_data = JSON.parse(received_bytes);
		const contorller_path = settings.pageFolder + req.url + '.cjs';
		//console.log(req.url);
		try{
			const {Controller} = await import(contorller_path);
			const controller = new Controller();
			const response = await controller.post(req_data);
			res.write(JSON.stringify(response));
		} catch (e){
			console.error('error', e);
			return;
		}
	})
	res.end();
}

const server = http.createServer(async (req, res) => {
	/** @var {Request} req */
	console.log('from thread', thread_options, req.url);

	const a = new Route();
	console.dir(a)

	res.end();
	//switch(req.method){
		//case 'GET':
			//processGet(req, res)
			//break;
		//case 'POST':
			//processPost(req, res)
			//break;
	//}
});

if(thread_options.host && thread_options.port) {
	server.listen(thread_options.port, thread_options.host, async () => {
	    console.log(`Server running at http://${thread_options.host}:${thread_options.port}/`);
	});
}

