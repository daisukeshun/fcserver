class Route {
    get #id(){
        return this.#id;
    };
    get #method(){
        return this.#method;
    };
    set #method(value){
        if(typeof value === "string"){
            this.#method = value.toLowerCase();
        }
    };
    get #url(){
        return this.#url;
    };
    set #url(value){
        if(typeof value === "string"){
            this.#url = value.toLowerCase();
        }
    };
    get #mime(){
        return this.#mime;
    }; 
    set #mime(value){
        if(typeof value === "string"){
            this.#mime = value.toLowerCase();
        }
    }; 
    constructor(method = 'get', url = '/', mime = 'text'){
        this.method = method.toLowerCase();
        this.url = url;
        this.mime = mime;
        this.#id = null;
    }

    static load(id){
        //query for loading route by id
    }
}

export default Route;