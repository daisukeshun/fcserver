class Event {
    /** @type {Date}*/
    #start;             //timestamp
    /** @type {Date}*/
    #end;               //timestamp
    /** @type {Boolean}*/
    #status;            //bool
    /** @type {Date}*/
    #duration;          //timestamp
    /** @type {number}*/
    #interval;          //int       week_day | day_count | month_count | year_count
    /** @type {string}*/
    #intervalType;     //string    week | day | month | year | null

    get #id(){
        return this.#id;
    };


    get #start(){
        return this.#start;
    }

    /**@param {Date} value*/
    set #start(value){
        this.#start = value
    }

    get #end(){
        return this.#end;
    }
    /**@param {Date} value*/
    set #end(value){
        if(value < this.#start){
            console.error('end date is invalid');
            return;
        }
        this.#duration = value.getTime() - this.#start.getTime();
        this.#end = value
    }

    get #status(){
        return this.#status;
    }
    set #status(value){
        this.#status = value;
    }

    get #duration(){
        return this.#duration;
    }
    /**@param {number} value*/
    set #duration(value){
        if(value < 0){
            console.error('duration is invalid');
            return;
        }
        this.#end = new Date(this.#start.getTime() + this.#duration);
        this.#duration = value;
    }

    get #interval(){
        return this.#interval;
    }
    /**@param {number} value*/
    set #interval(value){
        if(this.#interval < 1){
            console.error('interval is invalid');
            return;
        }
        this.#interval = Math.round(value);
    }

    get #intervalType(){
        return this.#intervalType;
    }
    /**@param {string} value*/
    set #intervalType(value){
        if(['week', 'day', 'month','year'].indexOf(value) < 0){
            console.error('interval is invalid');
            return;
        }
        this.#intervalType = value;
    }

    constructor(){
        this.id = null;
        this.#start = new Date('now');
    }

    save() {
    }

    static load(id){
    }
}

export default Route;